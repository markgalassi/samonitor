\section{Model and Methods}
\subsection{Obtaining and Preparing Data}

Our data set is publicly available on the Kaggle web site:\\
\url{https://www.kaggle.com/datasets/adtysregita/stress?select=stress.csv}

This data set initially contained 2,343 rows and 116 columns in a csv
file. All 2,343 rows were usable and didn't contain any missing
values.  We only use 4 of the columns: the row number, the text, the
label, and the confidence.

\begin{figure}[h!]
  \center
  \includegraphics[scale=0.9]{matrixpics/DataSnip.png}
  \caption{Snippet of data from worrisome messages.}
\end{figure}

The label is a binary label that codes for stress or no stress. A
label of ``1'' indicates stress, whereas ``0'' does not. These numbers
were provided by professionals based on the text for that
row. Confidence is another professional-provided value that indicates
the confidence the expert holds in that label.  At this time SAM does
not use the confidence metric.

This data is then finally ready to feed into the Random Forest
Classifier that we wrote for this project, using the \emph{scikit learn}
framework. \parencite{amadebai2022,kessler2016testing}

\subsection{An Introduction to Random Forest Models}

Random Forest Classifiers and Random Forest Regression are both
methods of machine learning. Random Forest models take numeric values
and feed them into decision trees. The trees then decide where this
data continues and then break into two or more trees. They inevitably
end up as decision leaves. These leaves are the final output of the
decision and are also numeric values. In the case of regressors, they
can be a range, why classifiers give a binary value.

Figure~\ref{fig:decision_forest} shows an example of a Random Forest
Regressor. It allows for many different outputs like apple, cherry, or
strawberry. It makes it's decisions based off of size.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.5]{matrixpics/RFRegress.png}
  \caption{Decision based off a Random Forest Regressor model.  Image
    borrowed from: \\
    \url{https://developer.nvidia.com/blog/accelerating-random-forests-up-to-45x-using-cuml/}}
  \label{fig:decision_forest}
\end{figure}
  
  
The following figure is an example of a Random Forest Classifier. It can only provide
a binary output, in this case yes or no. Our model uses a classifier
to give the output of stress or no stress.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.5]{matrixpics/RFClass.png}
  \caption{Example of random forest classifier.}
\end{figure}

\subsection{Implementation of a Random Forest Classifier}

We used a Random forest Classifier as our model type. It produces a 1
to indicate stress or a 0 to indicate no stress. Many programmers try
different model types: we deduced that Random Forest Classifier by far
is the most accepted model for natural language learning. We manipulated
two different ways of processing the data, and before it was fed
into the model.

\subsection{Intensity Score Method}

The first method of data processing was the Intensity Score method. We
wrote a dictionary of ``worrisome'' keywords, which are words the
literature suggests can indicate stress. Each word was given a
weight, and the weighed word occurrences are added.

The result was a training set consisting of a single numeric score for
each text snippet, and the ``expert label'' of stress or no stress for that snippet.

This method makes for a very simple initial implementation, but it
lacks nuance: the specific use of word pairs is lost in that single
score.

\subsection{Bag of Words Method}

The Bag of Words \parencite{gentleIntroBagWords} is far more inclusive
and more complex. The Bag of Words method reads through the text
snippet and accounts for the total number of times any word from the
English dictionary appears. It then scales the numbers by
importance. Importance is decided by the number of times each word
occurs, so words like the, as, and, then will carry far less
importance, and not affect the interpretation.

This method is far more reliable, because it accounts for all possible
words. However it does come with the downside of producing a sparse
vector. A sparse vector is a vector that contains many zeros, showing
no data. It can be useful if we see zeros in spots that would indicate
words like suicide, depression, or lonesomeness. It does become a
nuisance when zeros are always seen to indicate words like
Pneumonoultramicroscopicsilicovolcanoconiosis (considered the longest
word in English and is used for a certain lung disease).
