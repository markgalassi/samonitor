\section{Results}

Our final model accurately predicts if someone is at high levels of
stress based off of snippets of texts.  We were able to test the
applicability of our model by feeding it data sets of different sizes,
and having our Random Forest Classifier print various performance
metrics, namely the ``mean absolute error'' (MAE), the ``accuracy
score'', and for visualization purposes the ``confusion matrix''.

Our discussion of metrics is based on the ``dreaddit'' training (2838
samples) and testing (714 samples) data sets.

A confusion matrix is one of the ways to express if the classifier's
predictions were correct or incorrect. The confusion matrix generates
both actual and predicted values. All the diagonal elements denote
correctly classified outcomes. The misclassified outcomes are
represented in the off-diagonal portions of the confusion matrix. They
are commonly denoted as true positive, true negative, false positive,
and false negative.

In our application the main concern is the lower left entry in the
confusion matrix: when we predict ``OK'' but we should really have
been worried.  These are called ``false negatives'', and would lead
SAM to ignore a call for help.

\begin{center}
\begin{tabular}{||c c | c c||} 
 \hline
 in-sample MAE & in-sample accuracy & out-of-sample MAE &
 out-of-sample accuracy \\ [0.5ex] 
 \hline\hline
 0.0014 & 0.99859 & 0.30629 & 0.6937 \\ 
 \hline
\end{tabular}
\end{center}

\begin{figure}
  \centering
  \includegraphics[scale=1]{matrixpics/confusion_insample_bag-of-words.png}
  \caption{Simple example of confusion matrix for in-sample data on
    our training set.}
  \label{fig:confusion-matrix-example}
\end{figure}

In the confusion matrix in Figure~\ref{fig:confusion-matrix-example}
the top left value represents the {\bf True Negative} where the model
correctly predicted that the person is stressed. The top right value
is a {\bf False Positive} that says that the AI incorrectly predicted
that the person was stressed (when they were actually OK). The bottom
left is the {\bf False Negative}, the AI incorrectly predicted the
person to be ok (when they are actually stressed -- this value is the
key to whether our method works). The bottom right is a {\bf True
  Positive} where the model correctly predicted that the person is not
stressed.

We can run our model with either the bag-of-words method or the
intensity score method: the user can switch with a command-line
option. We use the Bag of Words method by default. Our model compares
its predictions to the actual results and produces the confusion
matrices. For either method, our model trained itself on a user
specified data set sample before it tested itself on data set sample
that it has never seen before. Using both methods we were able to
deduce that the Bag of Words method was more effective in correctly
classifying if someone was stressed or
not.

\begin{figure}[h!]
  \begin{subfigure}{.49\textwidth}
    \centering
    \includegraphics[scale=0.5]{matrixpics/confusion_insample_bag-of-words.png}
    \caption{Training Predictions}
  \end{subfigure}
  \begin{subfigure}{.49\textwidth}
    \centering
    \includegraphics[scale=0.5]{matrixpics/confusion_outofsample_bag-of-words.png}
    \caption{Testing Predictions}
  \end{subfigure}
  \caption{Bag of words predictions: here we look at the confusion
    matrix for both in-sample and out-of-sample data with the
    bag-of-words approach.}
  \label{fig:confusion-train-and-test}
\end{figure}

Figure~\ref{fig:confusion-train-and-test} shows the confusion matrix
for the Bag of Words method. In this instance the ``in-sample'' comes
from the dreaddit training data, and the ``out of sample'' comes
from the dreaddit test data.

\begin{figure}[!h]
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[scale=0.5]{matrixpics/confusion_insample_intensity-score.png}
    \caption{Training Predictions}
  \end{subfigure}
  \begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[scale=0.5]{matrixpics/confusion_outofsample_intensity-score.png}
    \caption{Testing Predictions}
  \end{subfigure}
  \caption{Intensity score approach: confusion matrix for in-sample
    and out-of-sample data.}
  \label{fig:intensity-score-approach}
\end{figure}


In Figure~\ref{fig:intensity-score-approach} we see what we get with
the ``intensity score'' approach, used on the same data sets.

The main take-home from these figures comes from the lower-left square
in the confusion matrix.  The in-sample ``false negative'' rate with
the bag-of-words approach is extremely low (0.00035, or 0.035\%).
With the intensity score approach it is much bigger (0.18, or 18\%).

The method performs worse with out-of-sample data: 0.076 (or 7.6\%)
false negatives for bag-of-words, versus 0.17 (17\%) false negatives
for the intensity score approach.

Clearly we can now focus on using the bag-of-words approach.
