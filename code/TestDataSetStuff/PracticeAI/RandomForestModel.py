# Pandas and numpy for data manipulation
import pandas as pd
import numpy as np

# No warnings about setting value on copy of slice
pd.options.mode.chained_assignment = None
pd.set_option('display.max_columns', 60)

# Matplotlib for visualization
import matplotlib.pyplot as plt
#matplotlib inline

# Set default font size
plt.rcParams['font.size'] = 24

from IPython.core.pylabtools import figsize

# Seaborn for visualization
import seaborn as sns
sns.set(font_scale = 2)

# Imputing missing values and scaling values
import sklearn.impute
from sklearn.preprocessing import MinMaxScaler
# Install with: pip3 install -U scikit-learn
# Machine Learning Models
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.svm import SVR
from sklearn.neighbors import KNeighborsRegressor

# Hyperparameter tuning
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV

def main():
   DataFrames()
       
def DataFrames():
    # Read in data into dataframes 
    TrainFeatures = pd.read_csv('Data/Training_Features.csv')
    test_features = pd.read_csv('Data/testing_features.csv')
    train_labels = pd.read_csv('Data/training_labels.csv')
    test_labels = pd.read_csv('Data/testing_labels.csv')
    
    # Display sizes of data
    print('Training Feature Size: ',TrainFeatures.shape)
    print('Testing Feature Size:  ', test_features.shape)
    print('Training Labels Size:  ', train_labels.shape)
    print('Testing Labels Size:   ', test_labels.shape)
main()
